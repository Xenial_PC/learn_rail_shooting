﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AliveTimer : MonoBehaviour
{
    float currentTime = 0, startingTime = 0;
    Text timeAliveText;

    void Start()
    {
        timeAliveText = GetComponent<Text>();
        currentTime = startingTime;
    }

    void Update()
    {
        timeAliveText.text = "Time Alive :" + currentTime.ToString("0");
        currentTime += 1 * Time.deltaTime;
    }
}

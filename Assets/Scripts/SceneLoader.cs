﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    //SerializedFields 
    [SerializeField] float levelLoadDely = 2f;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("LoadNextLevel", levelLoadDely); // Invoke LoadNext Level With a dely
    }

    void LoadNextLevel()
    {
        SceneManager.LoadScene(1); // loads the next scene
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRemoveTimer : MonoBehaviour
{
    //SerializedFields
    [Tooltip("Object Destruction Timer")][SerializeField] float objDestructionTime = 5f;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, objDestructionTime);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

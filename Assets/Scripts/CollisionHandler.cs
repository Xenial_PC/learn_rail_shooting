﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour
{
    //SerializedFields
    [Tooltip("In Seconds")][SerializeField]float levelLoadDely = 1f;
    [Tooltip("Particle Effects Prefab")][SerializeField]GameObject deathFX;

    public void OnTriggerEnter(Collider hit)
    {
        StartDeathSequence();
        Invoke("OnDeathReloadLevel", levelLoadDely);
        deathFX.SetActive(true);
    }

    private void StartDeathSequence()
    {
        SendMessage("OnPlayerDeath");
    }

    private void OnDeathReloadLevel()
    {
        SceneManager.LoadScene(1);
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityScript.Steps;

public class Enemy : MonoBehaviour
{
    //SerializedFields
    [SerializeField] GameObject enemyDeathFX;
    [SerializeField] Transform parent;
    [Tooltip("Score On Enemy Death")] [SerializeField] int scorePerHit = 1;
    [SerializeField] int hits = 10;

    ScoreBoard scoreBoard;

    void Start()
    {
        AddNonTriggerBoxCollider();
        scoreBoard = FindObjectOfType<ScoreBoard>();
    }

    private void AddNonTriggerBoxCollider()
    { 
        Collider noTrigger_BoxCollider = gameObject.AddComponent<BoxCollider>();
        noTrigger_BoxCollider.isTrigger = false;
    }

    void OnParticleCollision(GameObject other)
    {
        hits = hits - 1;
        if (hits < 1)
        {
            KillEnemy();
        }
        else { }
    }

    private void KillEnemy()
    {
        scoreBoard.ScoreHit(scorePerHit);
        GameObject fx = Instantiate(enemyDeathFX, transform.position, Quaternion.identity);
        fx.transform.parent = parent;
        Destroy(this.gameObject);
    }
}


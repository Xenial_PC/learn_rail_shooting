﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour
{
    //Serializedfields
    [Header("General")]
    [Tooltip("In ms^-1")] [SerializeField]float xSpeed = 10f;
    [Tooltip("In ms^1")] [SerializeField]float ySpeed = 10f;
    [Tooltip("In ms^-1 Left")] [SerializeField]float posL = -5.62f;
    [Tooltip("In ms^1 Right")] [SerializeField]float posR = 5.55f;
    [Tooltip("In Down")] [SerializeField]float posD = -2.659f;
    [Tooltip("in Up")] [SerializeField]float posU = 2.52f;
    [SerializeField] GameObject[] guns;

    [Header("Screen-POS/Factor")]
    [SerializeField]float positionYawFactor = 5f;
    [SerializeField]float controlYawFactor = 5f;

    [Header("Control-Pitch Roll")]
    [SerializeField]float positionRollFactor = 0f;
    [SerializeField]float controlRollFactor = -18f;

    [Header("Control-Pitch Factor")]
    [SerializeField] float positionPitchFactor = -5f;
    [SerializeField] float controlPitchFactor = -20f;

    float xPosClamped, yPosClamped;
    float xThrow, yThrow, zThrow;

    bool isControlEnabled = true;

    ParticleSystem particleSystem;

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (isControlEnabled)
        {
            XMovement();
            YMovement();
            ProcessRotation();
            ProcessFiring();
        }
    }

    void OnPlayerDeath() // called by string reference
    {
        isControlEnabled = false;
    }

    private void ProcessRotation()
    {
        zThrow = 0f;
        float pitch = transform.localPosition.y * positionPitchFactor + yThrow * controlPitchFactor,
        yaw = transform.localPosition.x * positionYawFactor + zThrow * controlYawFactor,
        roll = transform.localPosition.x * positionRollFactor + xThrow * controlRollFactor;
        transform.localRotation = Quaternion.Euler(pitch, yaw, roll);
    }

    private void XMovement()
    {
        xThrow = CrossPlatformInputManager.GetAxis("Horizontal");
        float xOffset = xThrow * xSpeed * Time.deltaTime;
        float rawXPos = transform.localPosition.x + xOffset;
        xPosClamped = Mathf.Clamp(rawXPos, posL, posR);
        transform.localPosition = new Vector3(xPosClamped, transform.localPosition.y, transform.localPosition.z);
    }

    private void YMovement()
    {
        yThrow = CrossPlatformInputManager.GetAxis("Vertical");
        float yOffset = yThrow * ySpeed * Time.deltaTime;
        float rawYPos = transform.localPosition.y + yOffset;
        yPosClamped = Mathf.Clamp(rawYPos, posD, posU);
        transform.localPosition = new Vector3(transform.localPosition.x, yPosClamped, transform.localPosition.z);
    }

    void ProcessFiring()
    {
        if (CrossPlatformInputManager.GetButton("Fire"))
        {
            SetGunsActive(true);
        }
        else
        {
            SetGunsActive(false);
        }
    }

    private void SetGunsActive(bool isActive)
    {
        foreach (GameObject gun in guns)
        {
            particleSystem = gun.GetComponent<ParticleSystem>();
            var emissionModule = particleSystem.emission;
            emissionModule.enabled = isActive;
        }
    }
}
